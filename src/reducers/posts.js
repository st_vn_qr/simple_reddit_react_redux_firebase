function posts(state={}, action) {
    switch (action.type) {
        case 'RECEIVE_POST':
            return Object.assign({}, state,
                { [action.post.key] : action.post })
        default:
            return state
    }

    return state
}

export default posts
