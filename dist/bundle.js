(function (React,ReactDOM,reactRedux,redux,firebase) {
    'use strict';

    React = 'default' in React ? React['default'] : React;
    ReactDOM = 'default' in ReactDOM ? ReactDOM['default'] : ReactDOM;
    firebase = 'default' in firebase ? firebase['default'] : firebase;

    function posts(state = {}, action) {
        switch (action.type) {
            case 'RECEIVE_POST':
                return Object.assign({}, state, { [action.post.key]: action.post });
            default:
                return state;
        }

        return state;
    }

    //import users from './users'

    var reducers = redux.combineReducers({
        posts //, users
    });

    function receivePost (post) {
        return {
            type: 'RECEIVE_POST',
            post
        };
    }

    function Post(props, context) {
        let post = props.post;

        let liked = post.likes && post.likes[context.user.uid];

        return React.createElement(
            'div',
            { className: 'post' },
            post.type === 'text' ? React.createElement(
                'div',
                null,
                React.createElement(
                    'div',
                    null,
                    post.author
                ),
                React.createElement(
                    'div',
                    null,
                    post.body
                )
            ) : React.createElement(
                'div',
                null,
                React.createElement(
                    'div',
                    null,
                    post.author
                ),
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'a',
                        { href: post.body.startsWith('http') ? post.body : 'http://' + post.body,
                            target: '_blank'
                        },
                        post.title ? post.title : post.body
                    )
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'button',
                    { onClick: dbLikePost.bind(null, post.key, context.user.uid, context.signIn) },
                    liked ? 'liked' : 'like'
                ),
                React.createElement(
                    'span',
                    null,
                    ' ',
                    post.likeCount || 0
                )
            )
        );
    }

    Post.contextTypes = {
        user: React.PropTypes.object,
        signIn: React.PropTypes.func
    };

    function dbLikePost(postID, userID, signIn) {
        if (!userID) {
            signIn();
            return;
        }

        let postRef = firebase.database().ref().child(`posts/${ postID }`);
        postRef.transaction(function (post) {
            if (post.likes) {
                if (!post.likes[userID]) {
                    post.likeCount++;
                    post.likes[userID] = true;
                }
            } else {
                post.likes = {
                    [userID]: true
                };
                post.likeCount = 1;
            }

            return post;
        });
    }

    class NewPost extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                type: 'text',
                body: ''
            };
        }

        changeBody(ev) {
            this.setState(Object.assign({}, this.state, {
                body: ev.target.value
            }));
        }

        changeRadio(type) {
            this.setState(Object.assign({}, this.state, {
                type
            }));
        }

        submit(ev) {
            let post = {
                type: this.state.type,
                body: this.state.body,
                author: this.context.user.displayName || 'anonymous',
                likeCount: 0
            };

            dbAddPost(post);

            ev.preventDefault();
            ev.stopPropagation();

            this.setState({
                type: 'text',
                body: ''
            });
        }

        render() {
            return React.createElement(
                'div',
                { className: 'new-post' },
                React.createElement(
                    'form',
                    { onSubmit: this.submit.bind(this) },
                    React.createElement(
                        'label',
                        null,
                        'text',
                        React.createElement('input', {
                            type: 'radio',
                            name: 'type',
                            onChange: this.changeRadio.bind(this, 'text'),
                            checked: this.state.type === 'text' })
                    ),
                    React.createElement(
                        'label',
                        null,
                        'link',
                        React.createElement('input', {
                            type: 'radio',
                            name: 'type',
                            onChange: this.changeRadio.bind(this, 'link'),
                            checked: this.state.type === 'link' })
                    ),
                    React.createElement('input', {
                        type: 'text',
                        name: 'body',
                        value: this.state.body,
                        onChange: this.changeBody.bind(this),
                        placeholder: this.state.type === 'text' ? 'text body' : 'link' }),
                    React.createElement('input', { type: 'submit', value: 'submit' })
                )
            );
        }
    }

    NewPost.contextTypes = {
        user: React.PropTypes.object
    };

    /**
     * @param post {object} post data
     * @param post.author {string} userID of submitter
     * @param post.type {string} either 'link' or 'text'
     * @param post.body {string} URI of link or text body
     */
    function dbAddPost(post) {
        firebase.database().ref().child('posts').push(post).then(() => console.log(`${ post.type } post pushed`));
    }

    // database

    firebase.database().ref().child('posts').on('child_added', dbOnPost);
    firebase.database().ref().child('posts').on('child_changed', dbOnPost);

    function dbOnPost(_post) {
        let post = _post.val();
        post.key = _post.key;

        store.dispatch(receivePost(post));
    }

    // auth

    let provider = new firebase.auth.GoogleAuthProvider();

    /// redux store

    let store = redux.createStore(reducers);

    function mapState(state) {
        return {
            posts: state.posts
        };
    }

    class redditClone extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                user: {
                    displayName: 'anonymous',
                    uid: null
                },
                signedIn: false
            };
        }

        getChildContext() {
            return {
                user: this.state.user,
                signIn: this.signIn.bind(this)
            };
        }

        signIn() {
            firebase.auth().signInWithPopup(provider).then(res => {
                this.setState({
                    user: res.user,
                    signedIn: true
                });
            });
        }

        render() {
            let posts = Object.keys(this.props.posts).map(postID => {
                return React.createElement(Post, { key: postID, post: this.props.posts[postID] });
            });

            return React.createElement(
                'div',
                null,
                React.createElement(
                    'h1',
                    null,
                    'r/all'
                ),
                this.state.signedIn ? null : React.createElement(
                    'button',
                    { id: 'signin', onClick: this.signIn.bind(this)
                    },
                    'sign in'
                ),
                React.createElement(NewPost, null),
                posts
            );
        }
    }

    redditClone.childContextTypes = {
        user: React.PropTypes.object,
        signIn: React.PropTypes.func
    };

    let RedditClone = reactRedux.connect(mapState)(redditClone);

    ReactDOM.render(React.createElement(
        reactRedux.Provider,
        { store: store },
        React.createElement(RedditClone, null)
    ), document.querySelector('mainApp'));

}(React,ReactDOM,ReactRedux,Redux,firebase));