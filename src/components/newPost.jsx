import React from 'react'
import firebase from 'firebase'

class NewPost extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            type: 'text',
            body: ''
        }
    }

    changeBody(ev) {
        this.setState(Object.assign({}, this.state, {
            body : ev.target.value
        }) )
    }

    changeRadio(type) {
        this.setState(Object.assign({}, this.state, {
            type
        }))
    }

    submit(ev) {
        let post = {
            type: this.state.type,
            body: this.state.body,
            author: this.context.user.displayName || 'anonymous',
            likeCount: 0
        }

        dbAddPost( post )

        ev.preventDefault()
        ev.stopPropagation()

        this.setState({
            type: 'text',
            body: ''
        })
    }

    render() {
        return (
            <div className="new-post">
                <form onSubmit={this.submit.bind(this)}>
                    <label>text
                        <input 
                            type="radio" 
                            name="type" 
                            onChange={this.changeRadio.bind(this, 'text')} 
                            checked={this.state.type === 'text'} />
                    </label>
                    <label>link
                        <input 
                            type="radio" 
                            name="type" 
                            onChange={this.changeRadio.bind(this, 'link')} 
                            checked={this.state.type === 'link'} />
                    </label>
                    <input 
                        type="text"
                        name="body"
                        value={this.state.body}
                        onChange={this.changeBody.bind(this)}
                        placeholder={ this.state.type === 'text' ? 
                            'text body' : 'link' } />
                    <input type="submit" value="submit" />
                </form>
            </div>
        )
    }
}

NewPost.contextTypes = {
    user : React.PropTypes.object
}

/**
 * @param post {object} post data
 * @param post.author {string} userID of submitter
 * @param post.type {string} either 'link' or 'text'
 * @param post.body {string} URI of link or text body
 */
function dbAddPost(post) {
    firebase.database().ref().child('posts').push(post).then(
        () => console.log(`${post.type} post pushed`)
    )
}

export default NewPost
