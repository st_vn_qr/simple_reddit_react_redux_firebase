import React from 'react'
import firebase from 'firebase'

function Post(props, context) {
    let post = props.post

    let liked = post.likes && post.likes[context.user.uid]

    return ( 
        <div className="post">
            { post.type === 'text' ?
                (
                    <div>
                        <div>{ post.author }</div>
                        <div>{ post.body }</div>
                    </div>
                )
                    :
                (
                    <div>
                        <div>{ post.author }</div>
                        <div>
                            <a href={ post.body.startsWith('http') ? 
                                post.body : 'http://' + post.body } 
                                target="_blank"
                                >{ post.title ? post.title : post.body }</a>
                        </div>
                    </div>
                )
            }


            <div>
                <button onClick={dbLikePost.bind(null, post.key, 
                                                 context.user.uid, 
                                                 context.signIn) }>{
                                                     liked ? 'liked' : 'like'
                                                 }</button>
                <span> { post.likeCount || 0 }</span>
            </div>
        </div>
    )
}

Post.contextTypes = {
    user : React.PropTypes.object,
    signIn : React.PropTypes.func
}

function dbLikePost(postID, userID, signIn) {
    if (!userID) {
        signIn()
        return
    }

    let postRef = firebase.database().ref().child(`posts/${postID}`)
    postRef.transaction( function(post) {
        if (post.likes) { 
            if (!post.likes[userID]) {
                post.likeCount++;
                post.likes[userID] = true
            }
        } else {
            post.likes = {
                [userID] : true
            }
            post.likeCount = 1
        }

        return post
    })
}

export default Post
