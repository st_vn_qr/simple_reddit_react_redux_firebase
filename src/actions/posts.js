export default function(post) {
    return {
        type: 'RECEIVE_POST',
        post
    }
}
