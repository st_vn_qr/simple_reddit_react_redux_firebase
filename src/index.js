import React from 'react'
import ReactDOM from 'react-dom'
import { Provider, connect } from 'react-redux'
import { createStore } from 'redux'

import reducers from './reducers'
import receivePost from './actions/posts'

import Post from './components/post'
import NewPost from './components/newPost'


/// firebase

import firebase from 'firebase'

// database

firebase.database().ref().child('posts').on('child_added', dbOnPost)
firebase.database().ref().child('posts').on('child_changed', dbOnPost)

function dbOnPost(_post) {
    let post = _post.val()
    post.key = _post.key

    store.dispatch( receivePost(post) )
}

// auth

let provider = new firebase.auth.GoogleAuthProvider()

/// redux store

let store = createStore( reducers )

function mapState(state) {
    return {
        posts: state.posts
    }
}

class redditClone extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            user : {
                displayName: 'anonymous',
                uid: null,
            },
            signedIn: false
        }
    }

    getChildContext() {
        return {
            user: this.state.user,
            signIn: this.signIn.bind(this)
        }
    }

    signIn() {
        firebase.auth().signInWithPopup(provider).then( res => {
            this.setState({
                user: res.user,
                signedIn: true
            })
        })
    }

    render() {
        let posts = Object.keys(this.props.posts)
            .map( postID => {
                return <Post key={postID} post={this.props.posts[postID]} />
            })

        return (
            <div>
                <h1>{ 'r/all' }</h1>
                { this.state.signedIn ? 
                    null : 
                        <button id="signin" onClick={this.signIn.bind(this)}
                            >sign in</button> 
                }
                <NewPost />
                { posts }
            </div>
        )
    }
}

redditClone.childContextTypes = {
    user: React.PropTypes.object,
    signIn : React.PropTypes.func
}

let RedditClone = connect( mapState )( redditClone )

ReactDOM.render(
    <Provider store={ store }>
        <RedditClone />
    </Provider>,
    document.querySelector( 'mainApp' )
)
