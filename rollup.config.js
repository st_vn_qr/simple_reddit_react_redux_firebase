import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'

export default {
    entry: 'src/index.js',
    external : ['firebase', 'react','react-dom','redux','react-redux'],
    globals : {
        ['react-redux'] : 'ReactRedux',
        'redux' : 'Redux'
    },
    plugins: [
        resolve({
            extensions : ['.js','.jsx','.json'],
            jsnext: true,
            browser: true
        }),
        commonjs(),
        replace({
            ['process.env.NODE_ENV'] : JSON.stringify('development')
        }),
        babel({
            exclude: 'node_modules/**',
            presets: ['react']
        })
    ],
    targets: [
        { dest: 'dist/bundle.js', format: 'iife' }
    ]
}
